import 'regenerator-runtime/runtime';
import { ProgressivePlayer } from "@accurate-player/accurate-player-progressive";
import { HotkeyPlugin } from "@accurate-player/accurate-player-plugins";
import "@accurate-player/accurate-player-controls";
import { Player } from '@accurate-player/accurate-player-core';
​import { getMediaInfo } from '@accurate-player/probe';
​
document.addEventListener("DOMContentLoaded", () => {
  // Reference to video element
  const videoElement = document.getElementById("video") as HTMLVideoElement;
​
  // Create a progressive player
  const player = new ProgressivePlayer(videoElement, (window as any).LICENSE_KEY);
  (window as any).player = player;

  // Enable hotkey plugin
  new HotkeyPlugin(player);
​
  // Use probe to load video file
  getMediaInfo(
  	"https://s3.eu-central-1.amazonaws.com/accurate-player-demo-assets/timecode/sintel-2048-timecode-stereo.mp4",{
  		label: "Sintel",
  		dropFrame: false
  }).then(file => {
      player.api.loadVideoFile(file);
  });

  // Initialize the UI controls
  initControls(videoElement, player);
});
​
// Creates the UI overlay controls on top of the video
function initControls(videoElement: HTMLVideoElement, player: Player): void {
  const controls = document.getElementsByTagName("apc-controls")[0] as any;
  if(!controls?.init) {
    setTimeout(() => {
      this.initControls(videoElement, player);
    }, 200);
    return;
  }
​
  controls.init(videoElement, player);
}